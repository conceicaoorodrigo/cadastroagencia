package br.com.capco.cadastroAgencia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CadastroAgenciaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CadastroAgenciaApplication.class, args);
		System.out.printf("Bem vindo ao cadastro de Agências Bancárias !!");
	}

}
