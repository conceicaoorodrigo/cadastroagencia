package br.com.capco.cadastroAgencia.controller.dto;

import br.com.capco.cadastroAgencia.modelo.Agencia;

import java.io.Serializable;
import java.time.LocalDateTime;

public class AgenciaDto implements Serializable {

    private Long id;
    private Integer numeroAgencia;
    private int telefone;
    private LocalDateTime dataCriacao;
    private EnderecoDto endereco;

    public AgenciaDto() {
    }

    public AgenciaDto(Agencia agencia){
        this.id = agencia.getId();
        this.numeroAgencia = agencia.getNumeroAgencia();
        this.telefone = agencia.getTelefone();
        this.dataCriacao = agencia.getDataCriacao();
        this.endereco = new EnderecoDto(agencia.getEndereco());
    }

    public Agencia toAgencia() {
        return new Agencia(this.numeroAgencia, this.telefone, this.endereco.toEndereco());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumeroAgencia() {
        return numeroAgencia;
    }

    public void setNumeroAgencia(Integer numeroAgencia) {
        this.numeroAgencia = numeroAgencia;
    }

    public int getTelefone() {
        return telefone;
    }

    public void setTelefone(int telefone) {
        this.telefone = telefone;
    }

    public LocalDateTime getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(LocalDateTime dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public EnderecoDto getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoDto endereco) {
        this.endereco = endereco;
    }
}
