package br.com.capco.cadastroAgencia;

import br.com.capco.cadastroAgencia.controller.CadastroAgenciaController;
import br.com.capco.cadastroAgencia.controller.dto.AgenciaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

@SpringBootTest(classes = CadastroAgenciaApplication.class)
public class CadastroAgenciaApplicationTests extends AbstractTestNGSpringContextTests {

	@Autowired
	private CadastroAgenciaController agenciaController;

	@Autowired
	private Object AgenciaDto;

	@Test
	public void recuperarPorNumeroAgencia() {
		Integer numeroAgenciaPesquisa = 1234;
		ResponseEntity resultado = agenciaController.recuperarPorNumeroAgencia(numeroAgenciaPesquisa);
		// resultado nao é um status NOT_FOUND
		Assert.assertNotEquals(resultado.getStatusCode(), HttpStatus.NOT_FOUND);
		// resultado é um 200-OK
		Assert.assertEquals(resultado.getStatusCode(), HttpStatus.OK);
		// resultado não tem o body vazio
		Assert.assertNotNull(resultado.getBody());
		// resultado é uma instancia de AgenciaDto
		Assert.assertTrue(resultado.getBody() instanceof AgenciaDto);
		// resultado tem realmente o numero da agencia que eu busquei
		AgenciaDto agenciaDto = (AgenciaDto) resultado.getBody();
		Assert.assertEquals(agenciaDto.getNumeroAgencia(), numeroAgenciaPesquisa);
	}

	@Test
	public void listarComOrdemCrescenteDeNumeroAgencia() {

		ResponseEntity listar = (ResponseEntity) agenciaController.listarComOrdemCrescenteDeNumeroAgencia(AgenciaDto);
	}

}
